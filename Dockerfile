FROM c5766/docker-emacs

ARG DEBIAN_FRONTEND=noninteractive

ENV PATH=$PATH:/usr/local/texlive/2019/bin/x86_64-linux

COPY texlive.profile /tmp/texlive.profile

RUN \
apt update && apt install -y \
wget \
libdbus-glib-1-2 \
&& \
cd /tmp && \
wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz  && \
tar xzvf install-tl-unx.tar.gz && \
cd /tmp/install-tl-20* && \
umask 022 && \
./install-tl -profile /tmp/texlive.profile && \
tlmgr install \
latex latex-bin luaotfload fontspec luatexja luatexbase \
ctablestack oberdiek xkeyval etoolbox adobemapping ms ipaex \
filehook biblatex biber graphics logreq url xstring  && \
rm -rf /tmp/*
